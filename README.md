![OS](https://badgen.net/badge/OS/Android?icon=https://raw.githubusercontent.com/androiddevnotes/awesome-android-kotlin-apps/master/assets/android.svg&color=3ddc84)
[![Kotlin](https://img.shields.io/badge/Kotlin-1.1.2-blue.svg)](http://kotlinlang.org)
![Language](https://img.shields.io/github/languages/top/cortinico/kotlin-android-template?color=blue&logo=kotlin)

# Hello Settings

<p align="center">
  <img src="app/src/main/ic_launcher-playstore.png" alt="Basic UI logo" width="80" height="80">
</p>


Simple Settings usage in <i>Android</i> with <i>Kotlin</i> language and <i>androix</i> dependency.

<i>PreferenceFragmentCompat</i> allow to easily create user settings screen
and <i>SharedPreferences</i> files allow to save key-value datas.

![PNG demo](app/src/main/screen_settings.png)

This is a showcase of simple settings app reading and writting in _SharedPreferences_.

<!-- TABLE OF CONTENTS -->
<details close>
  <summary>Table of Contents</summary>
  <ul>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#dependencies">Dependencies</a></li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#naming-convention">Naming convention</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#related-blog-posts-in-fr">Related blog posts in FR</a></li>
    <li><a href="#contact">Contact</a></li>
  </ul>
</details>

## Getting Started

This is an _Android_ project made with _Kotlin_ language.

### Prerequisites

Install [Android Studio](https://developer.android.com/studio), see instructions
on [ChillCoding.com](https://www.chillcoding.com/blog/2016/08/03/android-studio-installation/),
in FR.

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/chillcoding-at-the-beach/hellosettings.git
   ```
2. Choose, _Open an Existing Project_ in _Android Studio_

## Dependencies

### Library
  * [androix.preference](https://developer.android.com/reference/androidx/preference/package-summary)

## Usage

Use this space to show beautiful User Interface elements.

## Roadmap

See the [open issues](https://gitlab.com/chillcoding-at-the-beach/hellosettings/-/issues) for a list of proposed features (and known issues) or create one.

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## Naming convention

See naming convention of [Kotlin for Android project](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/-/wikis/Naming-Convention).

## License

Distributed under the GNU General Public License v3.0. See [LICENSE](https://github.com/machadaCosta/basic-ui/blob/main/LICENSE) for more information (cf. [Choose an open source license](https://choosealicense.com/)).

## Related blog posts in FR

 * [ChillCoding: Utiliser les fichiers de Préférences dans une application Android](https://www.chillcoding.com/blog/2014/10/10/utiliser-fichier-preferences/)

## Contact [![LinkedIn][linkedin-shield]][linkedin-url]

Macha DA COSTA, on [ChillCoding](https://www.chillcoding.com/?#about).

[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/MachaDaCosta/
