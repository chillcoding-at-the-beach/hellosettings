package com.chill.settings

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager

class MainActivity : AppCompatActivity() {

    val defaultSharedPref: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(this)
    }

    companion object {
        const val PREF_NB_OPENED = "NB_OPENED"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings, SettingsFragment())
                .commit()
        }
        trackAppOpening()
    }

    private fun trackAppOpening() {
        val editor: SharedPreferences.Editor = defaultSharedPref.edit()
        editor.putInt(PREF_NB_OPENED, defaultSharedPref.getInt(PREF_NB_OPENED, 0) + 1)
        editor.apply()
        Log.i(
            MainActivity::class.java.simpleName, "How many times app has been opened? " +
                    "${defaultSharedPref.getInt(PREF_NB_OPENED, 0)}"
        )
    }

    override fun onResume() {
        super.onResume()
        Log.i(
            MainActivity::class.java.simpleName, "SOUND value: " +
                    "${defaultSharedPref.getBoolean(getString(R.string.pref_sound), false)}"
        )
    }
}